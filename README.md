# ubuntu-rust

A Docker image based on Ubuntu including a Rust toolchain and tools.

Extend: [ubuntu-rust-mini](https://gitlab.com/docking/ubuntu-rust-mini)

Rust tools:
 - clippy
 - rustfmt
 - cargo-build-deps (from https://github.com/romac/cargo-build-deps)
 - cargo-cache
 - cargo-deb

Development tools:
 - build-essential (gcc, g++, make)
 - clang
 - cmake
 - git
 - kcov (from https://github.com/SimonKagstrom/kcov)
 - llvm
 - pkg-config
 - python-minimal

Development libraries:
 - binutils-dev
 - libclang-dev
 - libdw-dev
 - libiberty-dev
 - libjemalloc-dev
 - libpcap-dev
 - libssl-dev
 - zlib1g-dev

Misc. tools:
 - debsigs
 - unzip
 - zip

## Setup

* Install docker: https://docs.docker.com/engine/installation/
* Build the container image: `make build`
* Try the container: `make shell`

## Update

* Check [ubuntu-rust-mini](https://gitlab.com/docking/ubuntu-rust-mini) for matching base tag
* Create a new branch
```shell
git checkout -b nightly-2020-05-15
```
* Update the `RUST_VERSION` variable in `./Dockerfile` & `./Makefile`
```shell
sed -e 's!\(RUST_VERSION\)=.*$!\1=nightly-2020-05-15!' -i Dockerfile
sed -e 's!\(RUST_VERSION?\)=.*$!\1=nightly-2020-05-15!' -i Makefile
```
* Commit your changes
```shell
git add ./Dockerfile ./Makefile
git commit -m 'update(toolchain): switch to `nightly-2020-05-15`'
```
* Push the new branch
```shell
git push origin nightly-2020-05-15
```
* Wait for [CI/CD Pipelines](https://gitlab.com/docking/ubuntu-rust/pipelines) to finish & check result
* Create a new tag, matching `${CUSTOM_VERSION}-${UBUNTU_VERSION}-${RUST_VERSION}`
```shell
git tag 0.5-18.04-nightly-2020-05-15
```
* Push the new tag
```shell
git push origin 0.5-18.04-nightly-2020-05-15
```
* Wait for [CI/CD Pipelines](https://gitlab.com/docking/ubuntu-rust/pipelines) to finish & check result
* Check [Container Registry](https://gitlab.com/docking/ubuntu-rust/container_registry) for new tag
